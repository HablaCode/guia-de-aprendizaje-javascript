// javascript operadores matematicas

// adición

// console.log(10 + 5)

// let x = 10;
// let y = 5;
// let z = x + y;

// console.log(z)

// ---------------------- //

// sustracción

// let x = 10;
// let y = 5;
// let z = x - y;

// console.log(z);

// ---------------------- //

// también podemos sumar y restar con números negativos y flotantes (decimales)
// un numero flotante es un numero que contiene decimales

// asignar valores a "x" e "y" 
// let x = -5.2;
// let y = 2.5;

// // Resta y de x y asigna la diferencia a z
// let z = x - y;

// console.log(z);

// ---------------------- //

// algo interesante es que si sumas un numero y un string(cuerda) que contiene un numero "1"

let x = 1 + '1';

// en lugar de agregar los 2 numeros, Javascript convertira la declaracion entera a un string y relacionar guntos 

console.log(x);

console.log(typeof x);

// ---------------------- //

// Multiplicación y división