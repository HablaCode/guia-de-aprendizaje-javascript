// variables

// Un variable se usa para sostener datos, puedemos sostener datos del usario, visitantes, datos de informacion relacionado acompras, es un ejemplo

// Para crear un variable, usamos la palabra de clave "let"

let mensaje;

// Hora podemos darle un dato

mensaje = 'Hola';

// nuestro variable 'mensaje' hora guardara en el area de memoria asociada con el variable

// Podemos acceder el variable usando su nombre

console.log(mensaje)

// podemos declarar multiple variables en la misma linea

// let marca = 'ford',  morde = 'fusion', color = 'verde';

// lo de arriva sirve, pero ay que usar una mandera mas limpia

let marca = 'ford';
let morde = 'fusion';
let color = 'verde';

// cuando declarmos un variable y le damos un dato, podemos darle un nuevo dato y cuando pase eso eliminara el dato de antes, y guardara el dato nuevo

let shoe = 'nike';

console.log(shoe)

shoe = 'adidas'

console.log(shoe)

// hora podemos checkar que tipo de dato es nuestro variable shoe 

console.log(typeof shoe)

// declarando un variable dos vesos causa un error

let nombre = 'firulais';

nombre = 'carlos';

// let nombre = 'juan';

// SyntaxError: El identificador 'nombre' ya ha sido declarado

// ---------------- //

// como nombrar un variabla

// nombres validos
let ejemploUno;
let dato23;

// camelcase se puede utilizar cuando un variable tiene multiples palabras

// camelloCase

// tambien podemos usar unos symbolos como

let $ = 50;
let _ = 30;

console.log($)
console.log(_)

// ejemplos de nombres incorrectos

// let 1a; // no puede empesar con un number

// let my-name; // guiones no son permitidos

// nombres reservados no se pueden usar

// Por ejemplo let, class, return, y function son reservadas.

// let let = 2;

// console.log(let)

// ---------------- //

// const se declara cuando no quieras que un variable se cambie de dato

const fruta = 'naranja';

console.log(fruta);


// TypeError: Asignación a variable constante.

fruta = 'melon';

console.log(fruta);