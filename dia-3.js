
// puedes usar typeof para checkar a type de valor de un variable

typeof undefined // "undefined"

typeof 0 // "number"

typeof 10n // "bigint"

typeof true // "boolean"

typeof "foo" // "string"

typeof Symbol("id") // "symbol"

typeof Math // "object"  (1)

typeof null // "object"  (2)

typeof alert // "function"  (3)

// ---------------- //

// Tipo de conversiones

// string a numbero

let numbero = 'string';

console.log(typeof numbero);

numbero = Number(numbero)

console.log(typeof numbero);

// numero a string

let saludo = 5;

console.log(typeof saludo);

saludo = String(numbero)

console.log(typeof saludo);

// Boolean conversión

// El numero uno es un valor correcto
console.log(Boolean(1));

// valores que pueden ser vacios como '', null, undefined, and Nana son falsos
console.log(Boolean(0));

// el string tiene valor
console.log(Boolean('0')); // true

// el valor esta vacio
console.log(Boolean('')); // falso


// ---------------- //

// La conversion numerica ocurre en funciones y expresiones matematicas automaticanmente

console.log("6" / "2"); // falso


